<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) - Rohan Kulkarni
 * 
 * This file is released under the 3-clause BSD license. See COPYING-BSD.
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="restPUT" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <refnamediv>
    <refname>restPUT</refname>

    <refpurpose>Call a REST "PUT" method</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>result = restPUT(URL);
    result = restPUT(URL [, argsFromFile [, argsSrc [, authUser [, authPass [, resultDest [, headers]]]]]]);
    result = restPUT(URL [, options [, headers]]);
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>URL</term>
        <listitem>
          <para>String: An URL. Supported and tested: HTTP, HTTPS, FTP (IPv4 and IPv6)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>argsFromFile</term>
        <listitem>
          <para>Boolean optional parameter: Boolean value %t or %f. Describes the value in the argument argsSrc</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>argsSrc</term>
        <listitem>
          <para>String optional parameter: If argsFromFile = %t then it has the location to the file containing the arguments to be sent to the URL</para>
          <para>If argsFromFile = %f then it has the actual arguments to be sent</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>authUser</term>
        <listitem>
          <para>String optional parameter: Provide username for websites requiring HTTP authentication</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>authPass</term>
        <listitem>
          <para>String optional parameter: Provide password for websites requiring HTTP authentication</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>resultDest</term>
        <listitem>
          <para>String optional parameter: Provide the file/directory location for storing returned result from the URL</para>
        </listitem>
      </varlistentry>
      <varlistentry>
       <term>headers</term>
        <listitem>
         <para>Struct optional parameter: Scilab struct variable to provide custom Headers that need to be sent. Specified in the following format,</para>
         <para>headers = struct('header1', stringValue1, 'header2', stringValue2 ...)</para>
         <para>header1, header2... : Header names.</para>
         <para>stringValue: String variable specifying the corrosponding header value</para>
        </listitem>
      </varlistentry>
      <varlistentry>
       <term>options</term>
        <listitem>
          <para>Struct optional parameter: Scilab struct variable in the following format,</para>
          <para>options = struct('argsFromFile', boolValue, 'argsSrc', stringValue1, 'authUser', stringValue2, 'authPass', stringValue3, 'resultDest', stringValue4)</para>
          <para>stringValue: String variable specifying the corrosponding field value</para>
          <para>Note: All the fields in the options struct must be present.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>result</term>
          <listitem>
            <para>Returned content of the REST PUT call. Gives the location of file created if resultDest is specified</para>
          </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>restPUT will make the REST PUT call on the URL using the set options.</para> 

    <para>This function is based on the <ulink url="http://curl.haxx.se/libcurl/">curl library</ulink>.</para>
  
    <para> authUser and authPass can be specified for the websites which required authentication. The authentiction method used is the CURLAUTH_ANY provided by CURLOPT_HTTPAUTH parameter of the libcurl library.</para>

    <para> The headers specified will be appended to the default list of headers. If some of them are common with the existing ones, user specified values will be used.</para>

    <para> The return value, result, will have the data returned from the URL or the location of the file which has the data stored depending on whether resultDest was set. By default, the filename will be same as returned by the URL. However, if the filename is not provided by the URL itself, restPUT will rename the file to <emphasis role="italic">index.html</emphasis></para>

    <para>IPv6 (and obviously IPv4) are supported out-of-the box by restPUT.</para>

    <para>Proxy configuration is enabled. It uses the parameters from ATOMS config file.</para>

    <para>The <emphasis role="italic">CURLOPT_FOLLOWLOCATION</emphasis> curl option is activated to make sure the download follow the URL.</para>

    <para>Due to the fact that restPUT is based on libcurl, it is likely that <ulink url="http://curl.haxx.se/docs/features.html">other protocols</ulink> than HTTP, HTTPS and FTPS will work. However, they have not been tested enough.</para>

  </refsection>

  <refsection>
    <title>Examples</title>

    //Simple REST PUT
    result = restPUT("http://httpbin.org/put",%f,"PUT this data");

    //REST PUT, some args through the url, result stored in TMPDIR
    result = restPUT("http://httpbin.org/put?myarg=myvalue",%f,"PUT this data","","",TMPDIR);

    //REST PUT, using Options struct, custom Headers and PUT data from file
    srcFile = TMPDIR + "/restTemp";
    f1 = mopen(srcFile,"w");
    mputl("PUT this data",f1);
    mclose(f1);
    options = struct("argsFromFile",%t,"argsSrc",srcFile,"authUser","","authPass","","resultDest",TMPDIR);
    headers = struct("myheader","myheaderval","myheader2","myheaderval2");
    result = restPUT("http://httpbin.org/put",options,headers);

    <programlisting role="example">

    </programlisting>
  </refsection>

  <refsection role="see also">
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="restPOST">restPOST</link>
      </member>
      <member>
        <link linkend="restDELETE">restDELETE</link>
      </member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Rohan Kulkarni</member>
    </simplelist>
  </refsection>
</refentry>
