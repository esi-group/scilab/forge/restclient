// Copyright (C) 2012 - Rohan Kulkarni
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function builder_c()

  src_c_path = get_absolute_file_path("builder_c.sce");
  
  curl_path = '';
  curl_lib_path = '';

  files_c = ["restManager.c"];
  CFLAGS = '';
  LDFLAGS = '';

  if getos() <> "Windows" then

    [version,opts] = getversion();
    if opts(2) == "x86" then
      curl_path = src_c_path + "../../thirdparty/curl/linux/x86/";
    else 
      curl_path = src_c_path + "../../thirdparty/curl/linux/x64/";
    end

    curl_lib_path = curl_path + "lib/libcurl";

    CFLAGS = "-I/usr/include/libxml2/ -I" + curl_path + "/include -I" + src_c_path;
//    LDFLAGS = curl_path + "openssl";
  
  else    

    curl_path = src_c_path + "../../thirdparty/curl/windows/";
    CFLAGS = "-I" + fullpath(curl_path + "include") + ..
             " -I" + fullpath(src_c_path + "/../../thirdparty/libxml2/include/") + ..
             " -I" + fullpath(src_c_path);
    files_c = [files_c; ..
               "dllMain.c"; ..
               "isdir.c"; ..
               "isDrive.c"];
  end


tbx_build_src("restclient", ..
                  files_c, ..
                  "c", ..
                  src_c_path, ..
                  curl_lib_path, ..
                  LDFLAGS, ..
                  CFLAGS); 

endfunction

builder_c();
clear builder_c();
