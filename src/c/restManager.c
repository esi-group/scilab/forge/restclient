/*
 * Copyright (C) 2012 - Rohan Kulkarni
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef _MSC_VER
#define _GNU_SOURCE
#endif
#include<curl/curl.h>
#include <libxml/uri.h>
#include<string.h>
#include "Scierror.h"
#include "SCIHOME.h"
//#include "tmpdir.h"
#include "getos.h"
#include "PATH_MAX.h"
#include "MALLOC.h"
#include "BOOL.h"
#include "../c/isdir.h"
#include "charEncoding.h"
#include "../c/restManager.h"

#ifndef HAVE_BASENAME
static char *Curl_basename(const char *path);
#define basename(x)  Curl_basename((x))
#endif



static char errorBuffer[CURL_ERROR_SIZE];

struct inputString
{
    char *ptr;
    size_t len;
};

/**
 * Initialize inputString struct
 * @param s pointer to struct inputString to be initialized
 */
static void init_string(struct inputString *s)
{
    s->len = 0;
    s->ptr = (char*)CALLOC(s->len + 1, sizeof(char));
    if (s->ptr == NULL)
    {
	Scierror(999, "Internal error: calloc() failed.\n");
	return;
    }
}

/** 
 * FREE inputString struct
 * @param s pointer to struct inputString to be Freed
 */
void free_string(struct inputString *s)
{
    if(s != NULL)
    {
	s->len = 0;
	if(s->ptr!=NULL)FREE(s->ptr);
	s->ptr = NULL;
    }
}

/**
 * Free the memory allocated to the pointers
 */
void freeAllocatedMemory(char* proxyHost, char* proxyUserPass, char* authUserPass, struct inputString *s, struct curl_slist* chunk)
{
    if(proxyHost != NULL)
	FREE(proxyHost);
    if(proxyUserPass != NULL)
	FREE(proxyUserPass);
    if(authUserPass != NULL)
	FREE(authUserPass);
    if(chunk != NULL)
	FREE(chunk);
    free_string(s);

}

/**
 * Reads data from stream (Used by Curl read data)
 */
size_t readfunc(void *ptr, size_t size, size_t nmemb, void *stream)
{
    FILE *f = stream;
    size_t len;

    if (ferror(f))
	return CURL_READFUNC_ABORT;

    len = fread(ptr, size, nmemb, f) * size;

    return len;
}

/**
 * Writes data to string (Used by Curl write data)
 */
static size_t writefunc(void *ptr, size_t size, size_t nmemb, struct inputString *s)
{
    size_t new_len = s->len + size * nmemb;

    s->ptr = (char*)REALLOC(s->ptr, new_len + 1);
    if (s->ptr == NULL)
    {
	Scierror(999, "Internal error: realloc() failed.\n");
	return NULL;
    }
    memcpy(s->ptr + s->len, ptr, size * nmemb);
    s->ptr[new_len] = '\0';
    s->len = new_len;

    return size * nmemb;
}

/**
 * Get file name from URL
 * @param url the url
 * @return extracted filename
 */
static char *getFileNameFromURL(const char *url)
{
    char *filename = NULL;
    xmlURIPtr c = xmlParseURI(url);

    if (c == NULL)
    {
	Scierror(999, "Could not parse the URL.\n");
	return NULL;
    }

    if (c->path == NULL || strcmp(c->path, "/") == 0)
    {
	filename = (char *)MALLOC((strlen(DEFAULT_FILENAME) + 1) * sizeof(char));
	strcpy(filename, DEFAULT_FILENAME);
    }
    else
    {
	if (c->path == NULL)
	{
	    Scierror(43, "Internal error: c->path is null ?!\n");
	    return NULL;
	}

	char bname[PATH_MAX];
	strcpy(bname, basename(c->path));
	filename = (char *)MALLOC((strlen(bname) + 1) * sizeof(char));
	strcpy(filename, bname);
    }
    return filename;

}

/**
 * Sets ATOMS proxy values to Curl
 * @param curl pointer to the curl instance
 * @param proxyHost address to the proxy Host pointer(value is set in the function)
 * @param proxyUserPass address to the pointer storing user:pass of proxy host(value is set in the function)
 * @return 1 for success, proxy values assigned to the param pointers / 0 for failure
 */
int setRestProxy(CURL* curl, char** proxyHost, char** proxyUserPass)
{
    CURLcode res;
    FILE * pFile;
    long lSize;
    char *buffer;
    size_t result;
    char *configPtr;
    char *osName;
    char *osVer;
    char *tpPtr;

    char *host,*user,*password,*userpwd;
    long port = DEFAULT_PROXYPORT;

    int useproxy;

    char *tp, *field, *value, *eqptr;
    int eqpos,tplen;

    int userlen,passlen;

    /* construct ATOMS config file path */
    tpPtr = getSCIHOME();

    osName = getOSFullName();
    if(strcmp(osName,"Windows")==0)
    {

	osVer = getOSRelease();
	if(strstr(osVer,"x64")!=NULL)
	{
	    configPtr = (char *)MALLOC((strlen(tpPtr) + 19)*sizeof(char));
	    strcpy(configPtr,tpPtr);
	    strcat(configPtr,"/.atoms/x64/config");
	}
	else
	{
	    configPtr = (char *)MALLOC((strlen(tpPtr) + 15)*sizeof(char));
	    strcpy(configPtr,tpPtr);
	    strcat(configPtr,"/.atoms/config");
	}
	FREE(osVer);
    }
    else
    {
	configPtr = (char *)MALLOC((strlen(tpPtr) + 15)*sizeof(char));
	strcpy(configPtr,tpPtr);
	strcat(configPtr,"/.atoms/config");
    }

    wcfopen (pFile, configPtr , "rb" );

    FREE(tpPtr);
    FREE(configPtr);
    FREE(osName);

    if (pFile==NULL) 
    {
	//Proxy not set, Config file does not exist
	return 1;
    }

    fseek (pFile , 0 , SEEK_END);
    lSize = ftell(pFile);
    rewind (pFile);

    /* allocate memory to contain the whole file */
    buffer = (char*)MALLOC((lSize+1) * sizeof(char));
    if (buffer == NULL)  return 0;
    buffer[lSize]='\0';

    /* copy the file into the buffer */
    result = fread (buffer,1,lSize,pFile);
    if (result != lSize) 
    {
	Scierror(999, "Failed to read the scicurl_config file.\n");
	FREE(buffer);
	return 0;
    }

    host = user = password = userpwd = NULL;
    useproxy = 0;

    tp = field = value = eqptr = NULL;
    eqpos = tplen = 0;

    /* parse each line to extract variables */
    tp = strtok(buffer,"\n");
    while(tp!=NULL)
    {
	eqptr = strrchr(tp,'=');
	tplen = strlen(tp);
	if(eqptr==NULL)
	{
	    Scierror(999, "Improper syntax of scicurl_config file, '=' not found %d:%s\n",tplen,tp);
	    FREE(buffer);
	    if(host!=NULL)FREE(host);
	    if(user!=NULL)FREE(user);
	    if(password!=NULL)FREE(password);
	    return 0;
	}
	eqpos = eqptr - tp;
	if(tplen <= eqpos+1)
	{
	    Scierror(999, "Improper syntax of scicurl_config file, after an '='\n");
	    FREE(buffer);
	    if(host!=NULL)FREE(host);
	    if(user!=NULL)FREE(user);
	    if(password!=NULL)FREE(password);
	    return 0;
	}
	if(tp[eqpos-1]!=' ' || tp[eqpos+1]!=' ')
	{
	    Scierror(999, "Improper syntax of scicurl_config file, space before and after '=' expected\n");
	    FREE(buffer);
	    if(host!=NULL)FREE(host);
	    if(user!=NULL)FREE(user);
	    if(password!=NULL)FREE(password);
	    return 0;
	}

	/* get field and value from each line */
	field = (char *)MALLOC(sizeof(char)*(eqpos));
	value = (char *)MALLOC(sizeof(char)*(strlen(tp)-eqpos-1));

	memcpy(field,tp,eqpos-1);	field[eqpos-1]='\0';

	memcpy(value,tp+eqpos+2,strlen(tp)-eqpos-2);	value[strlen(tp)-eqpos-2]='\0';

	/* check and read proxy variables */
	if(strcmp(field,"useProxy")==0)
	{
	    if(strcmp(value,"False")==0)
	    {
		FREE(buffer);
		if(field!=NULL)FREE(field);
		if(value!=NULL)FREE(value);
		if(host!=NULL)FREE(host);
		if(user!=NULL)FREE(user);
		if(password!=NULL)FREE(password);
		return 1;
	    }
	    if(strcmp(value,"True")==0)
	    {
		useproxy = 1;
	    }	
	}
	else if(strcmp(field,"proxyHost")==0)	
	{
	    host = (char *)MALLOC((strlen(value)+1) * sizeof(char));
	    strcpy(host,value);
	}
	else if(strcmp(field,"proxyPort")==0)
	{
	    port = strtol(value,NULL,PROXYPORT_MAXLEN);
	}
	else if(strcmp(field,"proxyUser")==0)
	{	
	    user = (char *)MALLOC((strlen(value)+1) * sizeof(char));
	    strcpy(user,value);
	}
	else if(strcmp(field,"proxyPassword")==0)
	{
	    password = (char *)MALLOC((strlen(value)+1) * sizeof(char));
	    strcpy(password,value);
	}

	if(field!=NULL)FREE(field);
	if(value!=NULL)FREE(value);

	tp = strtok(NULL,"\n");
    }

    FREE(buffer);

    /* if proxy is set, update the parameters */
    if(useproxy==1)
    {
	/* proxyUserPwd = "user:password" */
	userlen = passlen = 0;
	if(user!=NULL)userlen = strlen(user);
	if(password!=NULL)passlen = strlen(user);
	if(userlen+passlen != 0)
	{
	    userpwd = (char *)MALLOC((userlen+passlen+2)*sizeof(char));
	    strcpy(userpwd,user); 
	    strcat(userpwd,":");
	    strcat(userpwd,password);

	    FREE(user); FREE(password);
	}

	res = curl_easy_setopt(curl,CURLOPT_PROXY,host);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed to set proxy host [%s]\n",errorBuffer);
	    FREE(userpwd); FREE(host);
	    return 0;
	}
	curl_easy_setopt(curl,CURLOPT_PROXYPORT,port);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed to set proxy port [%s]\n",errorBuffer);
	    FREE(userpwd); FREE(host);
	    return 0;
	}
	if(userpwd != NULL)
	{
	    res = curl_easy_setopt(curl,CURLOPT_PROXYUSERPWD,userpwd);
	    if(res != CURLE_OK)
	    {
		Scierror(999, "Failed to set proxy user:password [%s]\n",errorBuffer);
		FREE(userpwd); FREE(host);
		return 0;
	    }
	}
    }

    *proxyHost=host;
    *proxyUserPass=userpwd;

    fclose(pFile);
    return 1;
}

/**
 * Sets arguments to be sent to URL
 * @param curl pointer to the curl instance
 * @param argsFromFile boolean, read arguments from file
 * @param argsSrc pointer to arguments source
 * @param argsLen address of arguments length variable (value is set in the function)
 * @param argsFile address of arguments file pointer (value is set in the function)
 * @return 1 for OK, 0 for error
 */
int setRestArgs(CURL* curl, const BOOL argsFromFile, const char *argsSrc, int* argsLen, FILE** argsFile)
{
    CURLcode res;
    char* tmpFilePath = NULL;
    char* tpPtr = NULL;

    /* If Arguments present, store them in a tmp file, set cURL options to read them */
    if(argsSrc != NULL)
    {
	/* If Arguments present in argsSrc */
	if(argsFromFile == FALSE)
	{
	    /* Calculate arguments size */
	    *argsLen = strlen(argsSrc);

	    /* Create and Use restApiTmpData file in TMPDIR */
	    tpPtr = getSCIHOME();
	    
	    tmpFilePath = (char*)MALLOC((strlen(tpPtr) + 16 )*sizeof(char));	    
	    strcpy(tmpFilePath, tpPtr);
	    strcat(tmpFilePath, "/restApiTmpData");
	    
	    FREE(tpPtr);

	    wcfopen(*argsFile, tmpFilePath, "wb");
	    if(*argsFile == NULL)
	    {
		Scierror(999, "Failed opening a temp file in TMPDIR for writing arguments\n");
		FREE(tmpFilePath);
		return 0;
	    }
	    fwrite(argsSrc,1,*argsLen,*argsFile);
	    fclose(*argsFile);

	    /* Open argsFile in read mode, to pass it to CURLOPT_READDATA */
	    wcfopen(*argsFile, tmpFilePath, "rb");
	    FREE(tmpFilePath);
	    if (*argsFile == NULL)
	    {
		Scierror(999, "Failed opening restApiTmpData in TMPDIR for reading arguments\n");
		return 0;
	    }
	}
	else /* If the location of Arguments file present */
	{

	    /* Is argsSrc a directory path */
	    if(isdir(argsSrc))
	    {
		Scierror(999, "argsSrc is a directory path\n");
		return 0;
	    }

	    /* Open argsFile in read mode, to pass it to CURLOPT_READDATA */
	    wcfopen(*argsFile, argsSrc, "rb");
	    if (*argsFile == NULL)
	    {
		Scierror(999, "Failed opening authSrc for reading arguments\n");
		return 0;
	    }

	    /* Calculate arguments size */
	    fseek(*argsFile, 0, SEEK_END);
	    *argsLen = ftell(*argsFile);
	    rewind(*argsFile);
	}

	/* Set cURL options to set argsFile as arguments/data source */
	res = curl_easy_setopt(curl,CURLOPT_READFUNCTION,readfunc);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed to set readfunc [%s]\n",errorBuffer);
	    fclose(*argsFile);
	    return 0;
	}

	res = curl_easy_setopt(curl,CURLOPT_READDATA,*argsFile);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed to read arguments file [%s]\n",errorBuffer);
	    fclose(*argsFile);
	    return 0;
	}
    }
    else if(argsFromFile)
    {
	Scierror(999,"argsSrc cannot be NULL with argsFromFile set to %t");
	return 0;
    }
    return 1;
}

/**
 * Sets Curl authentication parameters
 * @param curl pointer to the curl instance
 * @param authUser authentication username
 * @param authPass authentication password
 * @param authUserPass address to the pointer storing user:pass of the url(value is set in the function)
 * @return 1 for OK, 0 for error
 */
int setRestAuth(CURL* curl, const char* authUser, const char* authPass, char** authUserPass)
{
    CURLcode res;
    int userPassLen = 0;
    char *userPass = NULL;
    if(authUser != NULL || authPass != NULL)
    {
	if(authUser != NULL)
	{
	    userPassLen = strlen(authUser);
	}
	if(authPass != NULL)
	{
	    userPassLen = userPassLen + strlen(authPass);
	}

	userPass = (char*)MALLOC((userPassLen + 2)*sizeof(char));

	strcpy(userPass,authUser);
	strcat(userPass,":");
	if(authPass != NULL)
	    strcat(userPass,authPass);

	/* Use CURLAUTH_ANY auth type to let cURL choose the best one */
	res = curl_easy_setopt(curl, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed to set httpauth type to ANY [%s]\n",errorBuffer);
	    FREE(userPass);
	    return 0;
	}

	res = curl_easy_setopt(curl, CURLOPT_USERPWD, userPass);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed to set user:pwd [%s]\n",errorBuffer);
	    FREE(userPass);
	    return 0;
	}
    }

    *authUserPass = userPass;

    return 1;
}

char* restPerform(const char* method, const char* url, const BOOL argsFromFile, const char* argsSrc, const char* authUser, const char* authPass, const char* resultDest, const char** headers, const int headerCount)
{
    CURL* curl;
    CURLcode res;

    int CHECK_OK;

    FILE* argsFile = NULL;
    int argsLen = 0;

    struct curl_slist* chunk = NULL;
    int iter = 0;

    FILE* resultFile;
    char* resultFilePath = NULL;

    char* proxyHost = NULL;
    char* proxyUserPass = NULL;
    char* authUserPass = NULL;

    char caInfo[PATH_MAX];
    char *tpCaInfo;

    char* name = NULL;
    struct inputString buffer;
    curl = curl_easy_init();

    if(curl)
    {

	init_string(&buffer);

	/* Set cURL errorBuffer */
	res = curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);
	if (res != CURLE_OK)
	{
	    Scierror(999, "Failed to set error buffer [%d]\n", res);
	    return NULL;
	}

	/* Set URL */
	res = curl_easy_setopt(curl, CURLOPT_URL, url);
	if (res != CURLE_OK)
	{
	    Scierror(999, "Failed to set URL [%s]\n", errorBuffer);
	    return NULL;
	}

	/* Set cURL proxy variables */
	CHECK_OK = setRestProxy(curl, &proxyHost, &proxyUserPass);
	if(!CHECK_OK)
	{
	    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
	    return NULL;
	}

	/* Set Arguments to URL */
	CHECK_OK = setRestArgs(curl, argsFromFile, argsSrc, &argsLen, &argsFile);
	if(!CHECK_OK)
	{
	    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
	    if(argsFile !=NULL)fclose(argsFile);
	    return NULL;
	}

	/* Set SSL Authenctication */
	res = curl_easy_setopt(curl,CURLOPT_SSL_VERIFYPEER,1);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed set SSL Verification [%s]\n",errorBuffer);
	    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
	    if(argsFile !=NULL)fclose(argsFile);
	    return NULL;
	}

	tpCaInfo = getSCIHOME();
	strcpy(caInfo,tpCaInfo);
	strcat(caInfo,"/ca-bundle.crt");
	FREE(tpCaInfo);
	res = curl_easy_setopt(curl, CURLOPT_CAINFO, caInfo);
	if(res != CURLE_OK)
	{
	    Scierror(999, "Failed to set CAINFO file path [%s]\n",errorBuffer);
	    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
	    if(argsFile !=NULL)fclose(argsFile);
	    return NULL;
	}

	/* Set cURL options for individual REST methods */
	if(strcmp(method, "GET") == 0)
	{
	    curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
	}
	else if(strcmp(method, "POST") == 0)
	{
	    curl_easy_setopt(curl, CURLOPT_POST, 1);

	    res = curl_easy_setopt(curl,CURLOPT_POSTFIELDSIZE,argsLen);
	    if(res != CURLE_OK)
	    {
		Scierror(999, "Failed to set POST field size [%s]\n",errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		return NULL;
	    }

	}
	else if(strcmp(method, "PUT") == 0)
	{
	    curl_easy_setopt(curl,CURLOPT_UPLOAD,1);

	    res = curl_easy_setopt(curl, CURLOPT_INFILESIZE, argsLen);
	    if(res != CURLE_OK)
	    {
		Scierror(999, "Failed to set PUT file size [%s]\n",errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		return NULL;
	    }
	}
	else if(strcmp(method, "DELETE") == 0)
	{
	    res = curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, method);
	    if(res != CURLE_OK)
	    {
		Scierror(999, "Failed to set CUSTOMREQUEST to DELETE [%s]\n",errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		return NULL;
	    }
	}

	/* Set cURL options for HTTP authentication */
	CHECK_OK = setRestAuth(curl, authUser, authPass, &authUserPass);
	if(!CHECK_OK)
	{
	    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
	    if(argsFile !=NULL)fclose(argsFile);
	    return NULL;
	}


	/* Set cURL headers, append to the existing ones */
	chunk = NULL;
	if(headerCount != 0)
	{
	    iter = 0;
	    while(iter != headerCount)
	    {
		chunk = curl_slist_append(chunk, headers[iter]);
		iter++;
	    }

	    res = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, chunk);
	    if(res != CURLE_OK)
	    {
		Scierror(999, "Failed to set custom headers [%s]\n",errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		return NULL;
	    }
	}

	/* Set cURL option to Follow redirects */
	curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION,1);

	/* If resultDest file location present */
	if(resultDest != NULL)
	{
	    if(isdir(resultDest))
	    {
		name = getFileNameFromURL(url);
		if(name == NULL)
		{
		    Scierror(999,"Failed to extract the result destination.\n");
		    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		    if(argsFile !=NULL)fclose(argsFile);
		    return NULL;
		}

		resultFilePath = (char*)MALLOC((strlen(resultDest) + strlen("/") + strlen(name) + 1)*sizeof(char));
		strcpy(resultFilePath, resultDest);
		strcat(resultFilePath, "/");
		strcat(resultFilePath, name);

		FREE(name);
	    }
	    else
	    {
		resultFilePath = (char*)MALLOC((strlen(resultDest)+1)*sizeof(char));
		strcpy(resultFilePath, resultDest);
	    }

	    wcfopen(resultFile,(char*)resultFilePath,"wb");

	    if (resultFile == NULL)
	    {
		Scierror(999, "Failed opening resultDest for writing\n");
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		FREE(resultFilePath);
		return NULL;
	    }

	    /* Set cURL data write function */
	    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, fwrite);
	    if (res != CURLE_OK)
	    {
		Scierror(999, "Failed to set write data function[%s]\n", errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		fclose(resultFile);
		FREE(resultFilePath);
		return NULL;
	    }

	    /* Get data to be written to file */
	    res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, resultFile);
	    if (res != CURLE_OK)
	    {
		Scierror(999, "Failed to set write data [%s]\n", errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		fclose(resultFile);
		FREE(resultFilePath);
		return NULL;
	    }

	    res = curl_easy_perform(curl);
	    if (res != 0)
	    {
		Scierror(999, "Transfer did not complete successfully: %s\n", errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsFile !=NULL)fclose(argsFile);
		fclose(resultFile);
		FREE(resultFilePath);
		return NULL;
	    }

	    /* always cleanup */
	    curl_easy_cleanup(curl);
	    curl_global_cleanup();

	    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);

	    fclose(resultFile);
	    if(argsSrc!=NULL)fclose(argsFile);

	    return resultFilePath;

	}
	else /* If resultDest not set, return data recieved */
	{

	    /* Set cURL data write function */
	    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writefunc);
	    if (res != CURLE_OK)
	    {
		Scierror(999, "Failed to set write data function[%s]\n", errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsSrc!=NULL)fclose(argsFile);
		return NULL;
	    }

	    /* Get data to be written to file */
	    res = curl_easy_setopt(curl, CURLOPT_WRITEDATA, &buffer);
	    if (res != CURLE_OK)
	    {
		Scierror(999, "Failed to set write data [%s]\n", errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsSrc!=NULL)fclose(argsFile);
		return NULL;
	    }

	    res = curl_easy_perform(curl);
	    if (res != 0)
	    {
		Scierror(999, "Transfer did not complete successfully: %s\n", errorBuffer);
		freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, &buffer, chunk);
		if(argsSrc!=NULL)fclose(argsFile);
		return NULL;
	    }

	    /* always cleanup */
	    curl_easy_cleanup(curl);
	    curl_global_cleanup();

	    if(argsSrc!=NULL)fclose(argsFile);

	    freeAllocatedMemory(proxyHost, proxyUserPass, authUserPass, NULL, chunk);

	    return buffer.ptr;
	}
    }
    else
    {
	Scierror(999, "Failed opening the curl handle.\n");
	return NULL;
    }

    return NULL;
}

/**
 * Get basename from URL path
 * @param path url path
 * @return extracted basename
 */
static char *Curl_basename(const char *path)
{
    char *s1 = NULL;
    char *s2 = NULL;

    s1 = strrchr(path, '/');
    s2 = strrchr(path, '\\');

    if(s1 && s2) 
    {
	path = (s1 > s2? s1 : s2)+1;
    }
    else if(s1)
    {
	path = s1 + 1;
    }
    else if(s2)
    {
	path = s2 + 1;
    }
    return path;
}
