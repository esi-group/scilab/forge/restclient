/* ==================================================================== */
/* Template REST client */
/* This file is released under the 3-clause BSD license. See COPYING-BSD. */
/* ==================================================================== */

#ifndef __RESTMANAGER_H__
#define __RESTMANAGER_H__

#include <stdio.h>
#include <stdlib.h>

#define DEFAULT_FILENAME "index.html"
#define OSNAME_MAXLEN 50
#define PROXYPORT_MAXLEN 10
#define DEFAULT_PROXYPORT 8080

/**
 * Perform REST call
 * @param method method of REST call
 * @param url url of the service
 * @param argsFromFile read arguments from file
 * @param argsSrc source of arguments
 * @param authUser authentication username of url
 * @param authPass authentication password of url
 * @param resultDest destination of results recieved
 * @param headers custom headers for the url
 * @param headerCount header count
 * @return the result recieved from url call
*/
char* restPerform(const char* method, const char *url, const BOOL argsFromFile, const char* argsSrc, const char* authUser, const char* authPass, const char* resultDest, const char **headers, const int headerCount);

#endif /* __RESTMANAGER_H__ */
