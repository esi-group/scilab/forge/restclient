/*
 * Copyright (C) 2012 - Rohan Kulkarni
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */
#include "restCALL.h"
#include "api_scilab.h"
#include "api_list.h"
#include "string.h"
#include "Scierror.h"
#include "MALLOC.h"
#include "localization.h"

/**
 * Gateway function for restDELETE. 
 * restDELETE can be called in two ways,
 * result = restDELETE(URL [, argsFromFile [, argsSrc [, authUser [, authPass [, resultDest [, headers]]]]]]);
 * result = restDELETE(URL [, options [, headers]]);
 *
 * The variables piAddressVar* in the function take respective values of the arguments passed by the user
 *
 * The expected variables and their types are,
 * URL: An URL string 
 * argsFromFile: A boolean, describes the value of argsSrc
 * argsSrc: A string, if argsFromFile = %t, it has the location of the file conatining the arguments to be sent to the URL
 *                    if argsFromFile = %f, it has the actual arguments to be sent
 *          Note: In restDELETE this should be '', the URL itself should contain the arguments.
 * authUser: A string, username for websites requiring HTTP authentication
 * authPass: A string, password for websited requiring HTTP authentication
 * resultDest: A string, directory/file location for storing the returned result
 * headers: A scilab struct, custom headers to be sent to the URL
 *          headers = struct('header1', stringValue1, 'header2', stringValue2 ...)
 *          header1, header2... : Header names
 *          stringValue: String variable specifying the corrosponding header value
 * options: A scilab struct specifying the options
 *          options = struct('argsFromFile', boolValue, 'argsSrc', stringValue1, 'authUser', stringValue2, 'authPass', stringValue3,
 *                    'resultDest', stringValue4)
 *          stringValue: String variable specifying the corrosponding field value
 *          Note: All the fields in the options struct must be present
 * result: A string, gives the result of the REST call (location of the file created if resultDest is specified)
 */
int sci_restDELETE(char *fname, int fname_len)
{
    SciErr sciErr;

    CheckLhs(0, 1);
    CheckRhs(1, 7);

    int *piAddressVarOne = NULL;
    int *piAddressVarTwo = NULL;
    int *piAddressVarThree = NULL;
    int *piAddressVarFour = NULL;
    int *piAddressVarFive = NULL;
    int *piAddressVarSix = NULL;
    int *piAddressVarSeven = NULL;

    char* method = NULL;
    char* retValue = NULL;

    int ret = 0;

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddressVarOne);
    if (sciErr.iErr)
    {
	printError(&sciErr, 0);
	return 0;
    }
    if(Rhs > 1)
    {
	sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddressVarTwo);
	if(sciErr.iErr)
	{
	    printError(&sciErr, 0);
	    return 0;
	}
    }
    if(Rhs > 2)
    {
	sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddressVarThree);
	if(sciErr.iErr)
	{
	    printError(&sciErr, 0);
	    return 0;
	}
    }
    if(Rhs > 3)
    {
	sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddressVarFour);
	if(sciErr.iErr)
	{
	    printError(&sciErr, 0);
	    return 0;
	}
    }
    if(Rhs > 4)
    {
	sciErr = getVarAddressFromPosition(pvApiCtx, 5, &piAddressVarFive);
	if(sciErr.iErr)
	{
	    printError(&sciErr, 0);
	    return 0;
	}
    }
    if(Rhs > 5)
    {
	sciErr = getVarAddressFromPosition(pvApiCtx, 6, &piAddressVarSix);
	if(sciErr.iErr)
	{
	    printError(&sciErr, 0);
	    return 0;
	}
    }
    if(Rhs > 6)
    {
	sciErr = getVarAddressFromPosition(pvApiCtx, 7, &piAddressVarSeven);
	if(sciErr.iErr)
	{
	    printError(&sciErr, 0);
	    return 0;
	}
    }

    method = (char*)MALLOC(sizeof(char)*7);
    strcpy(method,"DELETE");

    retValue = restCall(pvApiCtx, method, piAddressVarOne, piAddressVarTwo, piAddressVarThree, piAddressVarFour, piAddressVarFive, piAddressVarSix, piAddressVarSeven, fname);
   

    if (retValue != NULL)
    {
	/* create new variable */
	ret = createSingleString(pvApiCtx, Rhs + 1, retValue);
	FREE(retValue);
	retValue = NULL;
	if (ret)
	{
	    Scierror(999, _("%s: Could not create the output argument.\n"));
	    return 0;
	}
    }

    LhsVar(1) = Rhs + 1;
    PutLhsVar();
}
