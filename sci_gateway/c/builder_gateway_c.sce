// Copyright (C) 2012 - Rohan Kulkarni
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

LDFLAGS = '';
CFLAGS = ''
curl_path = '';
curl_lib_path = '';

   
if getos() == "Windows" then
    // to manage long pathname
    includes_src_c = "-I""" + fullpath(get_absolute_file_path("builder_gateway_c.sce") + "../../src/c") + """";
    CFLAGS = includes_src_c + " -I" + fullpath(get_absolute_file_path("builder_gateway_c.sce") + "../../thirdparty/libxml2/include/");
    CFLAGS = CFLAGS + " -I" + fullpath(get_absolute_file_path("builder_gateway_c.sce") + "../../thirdparty/curl/windows/include/");
else
    
    [version,opts] = getversion();
    if opts(2) == "x86"
        curl_path = get_absolute_file_path("builder_gateway_c.sce") + "../../thirdparty/curl/linux/x86/";
    else
        curl_path = get_absolute_file_path("builder_gateway_c.sce") + "../../thirdparty/curl/linux/x64/";
    end
    
    curl_lib_path = curl_path + "lib/libcurl";

    includes_src_c = "-I" + get_absolute_file_path("builder_gateway_c.sce") + "../../src/c -I/usr/include/libxml2/";
    CFLAGS = includes_src_c + " -I"+ curl_path + "/include/";
    LDFLAGS = "-lxml2 -g";
end

// PutLhsVar managed by user in sci_sum and in sci_sub
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
WITHOUT_AUTO_PUTLHSVAR = %t;

functions_gw = ["restPOST","sci_restPOST"; ..
                "restGET","sci_restGET"; ..
                "restPUT","sci_restPUT"; ..
		"restDELETE","sci_restDELETE"];
                
functions_files = ["sci_restPOST.c", ..
                    "sci_restGET.c", ..
                   "sci_restPUT.c", ..
		   "sci_restDELETE.c", ..
                    "restCALL.h", ..
                    "restCALL.c"];

if getos() == "Windows" then
  functions_files = [functions_files, "dllMain.c"];
end

tbx_build_gateway("restclient_gateway", ..
                  functions_gw, ..
                  functions_files, ..
                  get_absolute_file_path("builder_gateway_c.sce"), ..
                  ["../../src/c/librestclient"; curl_lib_path], .. 
                  LDFLAGS, ..
                  CFLAGS);

clear WITHOUT_AUTO_PUTLHSVAR;

clear tbx_build_gateway;
