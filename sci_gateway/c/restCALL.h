/*
 * Copyright (C) 2012 - Rohan Kulkarni 
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */


#ifndef __RESTCALL_H__
#define __RESTCALL_H__

/**
 * Common function for sci_rest* .c files to read Scilab variables and pass them to the restManager.c
 * @param pvApiCtx scilab environment variable
 * @param method rest method type
 * @param piAddressVarOne scilab address of parameter one passed through the scilab function call
 * @param piAddressVarTwo scilab address of parameter two passed through the scilab function call
 * @param piAddressVarThree scilab address of parameter three passed through the scilab function call
 * @param piAddressVarFour scilab address of parameter four passed through the scilab function call
 * @param piAddressVarFive scilab address of parameter five passed through the scilab function call
 * @param piAddressVarSix scilab address of parameter six passed through the scilab function call
 * @param piAddressVarSeven scilab address of parameter seven passed through the scilab function call
 * @return returned result from the restPerform call in the restManager.c
 */
char* restCall(void* pvApiCtx, char *method, int *piAddressVarOne, int *piAddressVarTwo, int *piAddressVarThree, int *piAddressVarFour, int *piAddressVarFive, int *piAddressVarSix, int *piAddressVarSeven, char *fname);

#endif
