//<-- NO CHECK REF -->

//REST DELETE
result = restDELETE("http://www.newburghschools.org/testfolder/dump.php?urlarg=urlval");
assert_checkfalse(grep(result, "urlarg") == []);


//REST DELETE, results stored in TMPDIR
result = restDELETE("http://httpbin.org/delete?myarg=myvalue",%f,"","","",TMPDIR);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
assert_checkfalse(grep(mgetl(f1),"myvalue") == []);
mclose(f1);


//REST DELETE, custom Headers
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restDELETE("http://httpbin.org/delete?myarg=myvalue",%f,"","","","",headers);
assert_checkfalse(grep(result,"myheaderval") == []);
assert_checkfalse(grep(result,"myheaderval2") == []);


//REST DELETE, using Options struct and custom Headers
options = struct("argsFromFile",%f,"argsSrc","","authUser","","authPass","","resultDest",TMPDIR);
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restDELETE("http://httpbin.org/delete?myarg=myvalue",options,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
assert_checkfalse(grep(fileContents,"myvalue") == []);
assert_checkfalse(grep(fileContents,"myheaderval") == []);
mclose(f1);
