//<-- NO CHECK REF -->


//Simple REST GET
result = restGET("http://httpbin.org/get?myarg=myvalue");
assert_checkfalse(grep(result,"myarg") == []);


//REST GET, result stored in TMPDIR
result = restGET("http://httpbin.org/get?myarg=myvalue",%f,"","","",TMPDIR);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
assert_checkfalse(grep(mgetl(f1),"myvalue") == []);
mclose(f1);


//REST GET, basic authentication
result = restGET("http://httpbin.org/basic-auth/user/passwd",%f,"","user","passwd");
assert_checkfalse(grep(result,"authenticated") == []);


//REST GET, HTTP Digest authentication
result = restGET("http://httpbin.org/digest-auth/auth/user/passwd",%f,"","user","passwd");
assert_checkfalse(grep(result,"authenticated") == []);


//REST GET, custom Headers added
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restGET("http://httpbin.org/get?myarg=myvalue",%f,"","","","",headers);
assert_checkfalse(grep(result,"myheaderval") == []);
assert_checkfalse(grep(result,"myheaderval2") == []);


//REST GET, using Options struct and custom Headers
options = struct("argsFromFile",%f,"argsSrc","","authUser","","authPass","","resultDest",TMPDIR);
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restGET("http://httpbin.org/get?myarg=myvalue",options,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"myvalue") == []);
assert_checkfalse(grep(fileContents,"myheaderval") == []);
